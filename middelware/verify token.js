const jwt = require('jsonwebtoken')

class BaseValidation {
    validateToken(req, res, next) {
        try {
            let token = req.headers['authorization']
            if (!token || (token && !token.startsWith("Bearer "))) {
                res.json("unauthorized request1")
            };
            token = token.slice(7, token.length);
            jwt.verify(token, process.env.JWT_SECRET, async (error, decoded) => {
                if (error) {
                    res.json("unauthorized request2")
                } else {
                    req.user = decoded
                    next()
                }
            });
        } catch (error) {
            res.json("unauthorized request3")

        }
    }
}


module.exports = new BaseValidation()


