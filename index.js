const express = require("express")
const mongoose = require("mongoose")
const { createServer } = require('http')
const app = express()
require('dotenv/config')
const PORT = 8560
const router = require('./routes')
app.use(express.json())
router(app)


let server = createServer(app)
server.listen(PORT, () => console.log('server on:', PORT))

mongoose.set('strictQuery', true)
mongoose.connect('mongodb://localhost:27017/todo', (err, data) => {

    if (err) {
        console.log("DB is not connected")
    } else {
        console.log("DB is connected")
    }
})


