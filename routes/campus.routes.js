const express = require('express')
const Campusroutes = express.Router()
const CAMPUS = require('../schema/campusSchema')

Campusroutes.post('/', async (req, res) => {
    try {
        const campus = await CAMPUS.create(req.body)
        res.send(campus)
    } catch (e) {
        res.send('Error In Create')
    }
})
Campusroutes.get('/get', async (req, res) => {
    try {
        const result = await CAMPUS.find()
        res.send(result);
    } catch (err) {
        res.send('Error In Get Data')
    }
})
Campusroutes.put('/update/:id', async (req, res) => {
    try {
        const update = await CAMPUS.updateOne({ _id: req.params.id }, req.body)
        res.send(update)
    } catch (e) {
        res.send('Error In Update')
    }
})
Campusroutes.put('/', async (req, res) => {
    try {
        const update = await CAMPUS.updateMany({ _id: { $in: req.body.id } }, { $set: { status: req.body.status } })
        console.log(update)
        res.send(update)
    } catch (e) {
        console.log(e)
        res.send('Error In Update')
    }
})
Campusroutes.delete('del/:id', async (req, res) => {
    try {
        const deleted = await CAMPUS.deleteOne();
        res.send(deleted)
    } catch (err) {
        res.send('Error In DeleteOne')
    }
})
Campusroutes.delete('/delete', async (req, res) => {
    try {
        const deleted = await CAMPUS.deleteMany({ _id: { $in: req.body.id } })
        res.send(deleted)
    } catch (err) {
        res.send('Error In DeleteMany')
    }
})

module.exports = Campusroutes