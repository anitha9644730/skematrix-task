const express = require('express')
const registerRoutes = express.Router()
const jwt = require('jsonwebtoken')
const User = require('../schema/userSchema')


registerRoutes.get('/', (req, res) => {
    User.find()
        .then(User => res.json(User))
        .catch(() => res.json('Error In Get Users'))
})
registerRoutes.post('/user', async (req, res) => {
    try {
        const { mobileno, mailid } = req.body
        const existingUser = await User.findOne({ $or: [{ mobileno }, { mailid }] })

        if (!existingUser) {
            const newUser = await User.create(req.body)
            res.status(201).json({ msg: 'Registered Successfully', data: newUser })
        } else {
            res.status(208).json({ msg: 'User already exists' })
        }
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Error in creating user' })
    }
})

registerRoutes.post('/login', async (req, res) => {
    try {
        const { user_name, password } = req.body
        const found = await User.findOne({
            $or: [
                { mailid: user_name },
                { mobileno: user_name }
            ]
        })

        if (!found) {
            return res.status(404).json({ error: 'User not found' })
        }

        if (found.password === password) {
            const token = jwt.sign({ userId: found._id }, process.env.JWT_SECRET)
            res.status(200).json({ msg: 'Login successful', data: token })
        } else {
            res.status(401).json({ error: 'Invalid password' })
        }
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Error in login' })
    }
});
module.exports = registerRoutes