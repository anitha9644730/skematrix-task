const initializeRoutes = (app) => {
    app.use('/campus', require('./campus.routes'))
    app.use('/register', require('./register.routes'))
    app.use('/building', require('./building.routes'))
}

module.exports = initializeRoutes