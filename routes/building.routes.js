const express = require('express')
const Buildingroutes = express.Router()
const Building = require('../schema/building.schema')

Buildingroutes.post('/', async (req, res) => {
    try {
        const building = await Building.create(req.body)
        res.send(building)
    } catch (e) {
        res.send('Error In Create')
    }
})
Buildingroutes.get('/get', async (req, res) => {
    try {
        const result = await Building.find()
        res.send(result)
    } catch (err) {
        res.send('Error In Get Data')
    }
})
Buildingroutes.put('/update/:id', async (req, res) => {
    try {
        const update = await Building.updateOne({ _id: req.params.id }, req.body)
        res.send(update)
    } catch (e) {
        res.send('Error In Update')
    }
})
Buildingroutes.put('/', async (req, res) => {
    try {
        const update = await Building.updateMany({ _id: { $in: req.body.id } }, { $set: { status: req.body.status } })
        console.log(update)
        res.send(update)
    } catch (e) {
        res.send('Error In Update Status')
    }
})
Buildingroutes.delete('del/:id', async (req, res) => {
    try {
        const deleted = await Building.deleteOne()
        res.send(deleted)
    } catch (err) {
        res.send('Error In DeleteOne')
    }
})
Buildingroutes.delete('/delete', async (req, res) => {
    try {
        const deleted = await Building.deleteMany({ _id: { $in: req.body.id } })
        res.send(deleted)
    } catch (err) {
        res.send('Error In DeleteMany')
    }
})

module.exports = Buildingroutes