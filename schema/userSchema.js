
const mongoose = require("mongoose")
const Schema = mongoose.Schema

const user_schema = new Schema({
    name: {
        type: String,
        required: true
    },
    mobileno: {
        type: String,
        required: true
    },
    mailid: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ['Y', 'N', 'D'],
        default: 'Y'
    }
},
    {
        collection: "col_user",
        timestamps: true
    }
)

module.exports = mongoose.model("User", user_schema)

