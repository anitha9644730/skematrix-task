const mongoose = require('mongoose')
const Schema = mongoose.Schema

const campus_schema = new Schema({
    name: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ["Y", "N", "D"],
        required: true
    }
},
    {
        collection: "campus",
        timestamps: true
    }
)

module.exports = mongoose.model("CAMPUS", campus_schema)