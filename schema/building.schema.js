const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const buliding_schema = new Schema({
    name: {
        type: String,
        require: true
    },
    campusId: {
        type: Schema.Types.ObjectId,
        require: true
    },
    status: {
        type: String,
        enum: ["Y", "N", "D"],
        required: true
    }
},
    {
        collection: "col_building",
        timestamps: true
    }
)

module.exports = mongoose.model("Building", buliding_schema)