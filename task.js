// 1. Write simple program for unique elements in array.
function findUniqueElements(arr) {
    let uniqueElements = []

    for (let element of arr) {
        if (!uniqueElements.includes(element)) {
            uniqueElements.push(element)
        }
    }

    return uniqueElements
}

const inputArray = [1, 3, 5, 3, 7, 1, 4, 3]
const uniqueArray = findUniqueElements(inputArray)
console.log("Input Array:", inputArray)
console.log("Unique Elements:", uniqueArray)

//2. Write a function that takes input two dates as string (in yyyy-mm - dd format) and calculates difference of dates between them.For simplicity purpose, assume you have 30 days for every month the second date is always after the first date.

function calculateDateDifference(date1, date2) {
    const [year1, month1, day1] = date1.split("-").map(Number)
    const [year2, month2, day2] = date2.split("-").map(Number)

    const daysInYear = 360
    const daysInMonth = 30

    const totalDays1 = year1 * daysInYear + month1 * daysInMonth + day1
    const totalDays2 = year2 * daysInYear + month2 * daysInMonth + day2

    const difference = totalDays2 - totalDays1

    return difference
}

const date1 = "2021-12-01"
const date2 = "2022-04-03"
const difference = calculateDateDifference(date1, date2)

console.log(`The difference in days between ${date1} and ${date2} is ${difference} days.`)


//3. Write simple program to find third largest element in array.

function findThirdLargest(arr) {
    if (arr.length < 3) {
        return "Array doesn't have enough elements."
    }

    arr = Array.from(new Set(arr))
    arr.sort((a, b) => b - a)

    return arr[2];
}

const array = [1, 3, 5, 3, 7, 1, 4, 3]
const thirdLargest = findThirdLargest(array)

console.log("Input Array:", array)
console.log("Third Largest Element:", thirdLargest)

//4. Write a program to move array elements to the left. The first element becomes the last element.
function moveElementsToLeft(arr) {
    if (arr.length <= 1) {
        return arr;
    }

    const firstElement = arr.shift();
    arr.push(firstElement);

    return arr;
}

const arr = [1, 3, 5, 7, 9, 11]
console.log("Original Array:", arr)
const movedArray = moveElementsToLeft(arr)

console.log("Moved Array:", movedArray)

//5. Schedule period is defined as day of week in number (Monday – 1, Tuesday – 2, ... Sunday – 7), from hour (1 – 24) and end hour (1-24). There is a schedule period available and an array of previous schedules are provided. You need to validate whether the new schedule can be added into the array if the new schedule is not overlapping with the existing schedule or the end hour is after start hour.

function validateSchedule(newSchedule, previousSchedules) {
    const { dayOfWeek, fromHour, toHour } = newSchedule


    if (toHour <= fromHour) {
        return "To hour should be after from hour"
    }

    for (const schedule of previousSchedules) {
        if (schedule.dayOfWeek === dayOfWeek) {
            if ((fromHour >= schedule.fromHour && fromHour < schedule.toHour) ||
                (toHour > schedule.fromHour && toHour <= schedule.toHour)) {
                return "Schedule overlaps with existing record"
            }
        }
    }

    return "Valid"
}


const previousSchedules = [
    { dayOfWeek: 1, fromHour: 7, toHour: 10 },
    { dayOfWeek: 2, fromHour: 9, toHour: 6 },

]

const newSchedule = { dayOfWeek: 1, fromHour: 9, toHour: 12 }

const validationMessage = validateSchedule(newSchedule, previousSchedules)
console.log(validationMessage)


//6. You are provided with two array of data. You have to calculate the matching score. Matching score is calculated using the below formula and rounded off to whole number.
function calculateMatchingScore(array1, array2) {
    const commonElements = array1.filter(item => array2.includes(item))
    const uniqueElements = new Set([...array1, ...array2])

    const commonCount = commonElements.length
    const uniqueCount = uniqueElements.size

    const score = (commonCount / uniqueCount) * 100
    return Math.round(score);
}


const array1 = [1, 2, 3, 4]
const array2 = [5, 6, 3, 2, 8]

const matchingScore = calculateMatchingScore(array1, array2)
console.log(`Matching Score: ${matchingScore}%`)
